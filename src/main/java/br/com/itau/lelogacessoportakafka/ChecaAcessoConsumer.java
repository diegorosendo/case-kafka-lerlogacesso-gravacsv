package br.com.itau.lelogacessoportakafka;

import br.com.itau.acesso.model.dto.ChecaAcesso;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.stereotype.Component;

import java.io.FileWriter;
import java.io.IOException;

@Component
public class ChecaAcessoConsumer {



    @KafkaListener(topics = "spec3-diego-rosendo-1", groupId = "Diego-zoom-mastertech1")
    public void receber(@Payload ChecaAcesso checaAcesso) {
        System.out.println("RECEBI LOG DE ACESSO: " + checaAcesso.getAcessoLiberado()
                            + " EM: " + checaAcesso.getDataHora()
                            + " DO CLIENTE: " + checaAcesso.getClienteId()
                            + " PARA PORTA: " + checaAcesso.getPortaId());

        System.out.println("Grava CSV");
        generateCsvFile("/home/a2/aula-espec3/AtividadeKafkaAcessoPorta/logchecaacesso.csv", checaAcesso);

    }

    private static void generateCsvFile(String sFileName, ChecaAcesso checaAcesso)
    {
        try
        {
            FileWriter writer = new FileWriter(sFileName, true);

            writer.append(checaAcesso.getClienteId().toString());
            writer.append(',');
            writer.append(checaAcesso.getPortaId().toString());
            writer.append(',');
            writer.append(checaAcesso.getDataHora().toString());
            writer.append(',');
            writer.append(checaAcesso.getAcessoLiberado().toString());

            writer.append('\n');

            writer.flush();
            writer.close();
        }
        catch(IOException e)
        {
            e.printStackTrace();
        }
    }


}
