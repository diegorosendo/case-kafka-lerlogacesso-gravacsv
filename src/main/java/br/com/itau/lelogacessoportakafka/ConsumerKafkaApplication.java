package br.com.itau.lelogacessoportakafka;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.io.FileWriter;

@SpringBootApplication
public class ConsumerKafkaApplication {

	public static void main(String[] args)
	{
		SpringApplication.run(ConsumerKafkaApplication.class, args);
	}

}
