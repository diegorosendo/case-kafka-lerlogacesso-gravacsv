package br.com.itau.acesso.model.dto;

import java.time.LocalDateTime;

public class ChecaAcesso {
    private Long clienteId;
    private Long portaId;
    private LocalDateTime dataHora;
    private Boolean acessoLiberado;

    public ChecaAcesso() {
    }

    public Long getClienteId() {
        return clienteId;
    }

    public void setClienteId(Long clienteId) {
        this.clienteId = clienteId;
    }

    public Long getPortaId() {
        return portaId;
    }

    public void setPortaId(Long portaId) {
        this.portaId = portaId;
    }

    public LocalDateTime getDataHora() {
        return dataHora;
    }

    public void setDataHora(LocalDateTime dataHora) {
        this.dataHora = dataHora;
    }

    public Boolean getAcessoLiberado() {
        return acessoLiberado;
    }

    public void setAcessoLiberado(Boolean acessoLiberado) {
        this.acessoLiberado = acessoLiberado;
    }
}
